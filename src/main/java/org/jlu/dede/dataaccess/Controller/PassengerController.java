package org.jlu.dede.dataaccess.Controller;

import io.protostuff.Request;
import org.jlu.dede.dataaccess.exceptions.IdNotExistException;
import org.jlu.dede.dataaccess.exceptions.NullBodyException;
import org.jlu.dede.dataaccess.model.Order;
import org.jlu.dede.dataaccess.model.Passenger;
import org.jlu.dede.dataaccess.service.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/passengers")
public class PassengerController {
    @Autowired
    PassengerService passengerService;
    @PostMapping
    @ResponseBody
    public void addPassenger(@RequestBody Passenger passenger)throws NullBodyException {
        if(null == passenger){
            throw new NullBodyException();
        }
        passengerService.addPassenger(passenger);
    }
    @Modifying
    @PostMapping("/{id}")
    @ResponseBody
    public void updatePassenger(@PathVariable Integer id,@RequestBody Passenger passenger)throws IdNotExistException,NullBodyException{
        if(null == passenger){
            throw new NullBodyException();
        }
        passenger.setId(id);
        passengerService.updatePassenger(id,passenger);
    }
    @GetMapping("/{id}")
    @ResponseBody
    public Passenger getByID(@PathVariable Integer id)throws IdNotExistException{
        return passengerService.findById(id);
    }
    @PostMapping("/{id}/order")
    @ResponseBody
    public Integer passengerOrder(@PathVariable Integer id, @RequestBody Order order)throws IdNotExistException,NullBodyException{
        return passengerService.passengerOrder(id,order);
    }
    @GetMapping("/account/{id}")
    @ResponseBody
    public Passenger findByAccount(@PathVariable Integer id){
        return passengerService.findByAccount(id);
    }
    @GetMapping("/order/{id}")
    @ResponseBody
    public Passenger findByOrder(@PathVariable Integer id){
        return passengerService.findByOrder(id);
    }
}
