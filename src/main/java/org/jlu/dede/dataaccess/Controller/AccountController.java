package org.jlu.dede.dataaccess.Controller;

import org.jlu.dede.dataaccess.exceptions.IdNotExistException;
import org.jlu.dede.dataaccess.exceptions.NullBodyException;
import org.jlu.dede.dataaccess.model.Account;
import org.jlu.dede.dataaccess.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/accounts")
public class AccountController {
    @Autowired
    AccountService accountService;
    @PostMapping
    @ResponseBody
    public void addAccount(@RequestBody Account account)throws NullBodyException{
        if (null == account){
            throw(new NullBodyException());
        }
        accountService.save(account);
    }
    @PostMapping("/{id}")
    @ResponseBody
    public void updateAccount(@PathVariable Integer id,@RequestBody Account account)throws NullBodyException, IdNotExistException {
        if (null == account){
            throw(new NullBodyException());
        }
        accountService.updateAccount(id,account);
    }
    @GetMapping("/{id}")
    @ResponseBody
    public Account getByID(@PathVariable Integer id)throws IdNotExistException{
        return accountService.findById(id);
    }
    @GetMapping("/email/{emailname}")
    @ResponseBody
    public Account getByEmail(@PathVariable String emailname){
        return accountService.findByEmail(emailname);
    }
    @GetMapping("/passenger/{id}")
    @ResponseBody
    public Account getByPassenger(@PathVariable Integer id){
        return accountService.findByPassenger(id);
    }
    @GetMapping("/driver/{id}")
    @ResponseBody
    public Account getByDriver(@PathVariable Integer id){
        return accountService.findByDriver(id);
    }
}
