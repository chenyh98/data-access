package org.jlu.dede.dataaccess.Controller;

import org.jlu.dede.dataaccess.exceptions.IdNotExistException;
import org.jlu.dede.dataaccess.exceptions.NullBodyException;
import org.jlu.dede.dataaccess.model.Car;
import org.jlu.dede.dataaccess.model.Driver;
import org.jlu.dede.dataaccess.model.Order;
import org.jlu.dede.dataaccess.model.Passenger;
import org.jlu.dede.dataaccess.service.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/drivers")
public class DriverController {
    @Autowired
    DriverService driverService;
    @PostMapping
    @ResponseBody
    public void addDriver(@RequestBody Driver driver)throws NullBodyException {
        if(null == driver){
            throw new NullBodyException();
        }
        driverService.addDriver(driver);
    }
    @PostMapping("/{id}")
    @ResponseBody
    public void updateDriver(@PathVariable Integer id,@RequestBody Driver driver)throws IdNotExistException,NullBodyException {
        if(null == driver){
            throw new NullBodyException();
        }
        driverService.updateDriver(id,driver);
    }
    @GetMapping("/{id}")
    @ResponseBody
    public Driver getByID(@PathVariable Integer id)throws IdNotExistException,NullBodyException{
        return driverService.findById(id);
    }
    @PostMapping("/{id}/order")
    @ResponseBody
    public boolean driverOrder(@PathVariable Integer id, @RequestBody Order order)throws IdNotExistException,NullBodyException{
        driverService.driverOrder(id,order);
        return true;
    }
    @PostMapping("/{id}/car")
    @ResponseBody
    public void chooseCar(@PathVariable Integer id,@RequestBody Car car)throws IdNotExistException,NullBodyException{
        if(null == car){
            throw new NullBodyException();
        }
        driverService.chooseCar(id,car);
    }
    @GetMapping("/{id}/state/{state}")
    @ResponseBody
    public void updateState(@PathVariable Integer id,@PathVariable String state)throws IdNotExistException{
        driverService.updateState(id,state);
    }
    @GetMapping("/account/{id}")
    @ResponseBody
    public Driver findByAccount(@PathVariable Integer id){
        return driverService.findByAccount(id);
    }
    @GetMapping("/order/{id}")
    @ResponseBody
    public Driver findByOrder(@PathVariable Integer id){
        return driverService.findByOrder(id);
    }
}
