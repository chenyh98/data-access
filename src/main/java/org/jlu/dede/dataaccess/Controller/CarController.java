package org.jlu.dede.dataaccess.Controller;

import org.jlu.dede.dataaccess.exceptions.IdNotExistException;
import org.jlu.dede.dataaccess.exceptions.NullBodyException;
import org.jlu.dede.dataaccess.model.Car;
import org.jlu.dede.dataaccess.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cars")
public class CarController {
    @Autowired
    CarService carService;
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public void deleteCar(@PathVariable Integer id)throws IdNotExistException {
        carService.deleteById(id);
    }
    @PostMapping
    @ResponseBody
    public void addCar(@RequestBody Car car)throws NullBodyException {
        if(null == car){
            throw new NullBodyException();
        }
        carService.addCar(car);
    }
    @PostMapping("/{id}")
    @ResponseBody
    public void updateCar(@PathVariable Integer id,@RequestBody Car car)throws IdNotExistException,NullBodyException{
        if(null == car){
            throw new NullBodyException();
        }
        carService.updateCar(id,car);
    }
    @GetMapping("/{id}")
    @ResponseBody
    public Car getByID(@PathVariable Integer id)throws IdNotExistException{
        return carService.getById(id);
    }
    @GetMapping("/driver/{id}")
    @ResponseBody
    public List<Car> getByDriver(@PathVariable Integer id){
        return carService.findByDriver(id);
    }
}
