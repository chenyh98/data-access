package org.jlu.dede.dataaccess.Controller;

import org.jlu.dede.dataaccess.exceptions.IdNotExistException;
import org.jlu.dede.dataaccess.exceptions.NullBodyException;
import org.jlu.dede.dataaccess.model.Record;
import org.jlu.dede.dataaccess.service.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/records")
public class RecordController {
    @Autowired
    RecordService recordService;
    @PostMapping
    @ResponseBody
    public void addRecord(@RequestBody Record record)throws NullBodyException {
        if(null == record){
            throw new NullBodyException();
        }
        recordService.addRecord(record);
    }
    @GetMapping("/{id}")
    @ResponseBody
    public Record getByID(@PathVariable Integer id)throws IdNotExistException{
        return recordService.findById(id);
    }
    @GetMapping("/payer/{id}")
    @ResponseBody
    public List<Record> findByPayer(@PathVariable Integer id){
        return recordService.findByPayer(id);
    }
}
