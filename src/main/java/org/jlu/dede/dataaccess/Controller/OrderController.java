package org.jlu.dede.dataaccess.Controller;

import com.netflix.discovery.converters.Auto;
import org.jlu.dede.dataaccess.exceptions.IdNotExistException;
import org.jlu.dede.dataaccess.exceptions.NullBodyException;
import org.jlu.dede.dataaccess.model.Order;
import org.jlu.dede.dataaccess.repository.DriverRepository;
import org.jlu.dede.dataaccess.repository.PassengerRepository;
import org.jlu.dede.dataaccess.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {
    @Autowired
    OrderService orderService;
    @Autowired
    PassengerRepository passengerRepository;
    @Autowired
    DriverRepository driverRepository;
    @PostMapping
    @ResponseBody
    public Integer addOrder(@RequestBody Order order)throws NullBodyException {
        if(null == order){
            throw new NullBodyException();
        }
        return orderService.addOrder(order);
    }
    @PostMapping("/{id}")
    @ResponseBody
    public boolean updateOrder(@PathVariable Integer id,@RequestBody Order order)throws IdNotExistException,NullBodyException{
        if(null == order){
            throw new NullBodyException();
        }
        orderService.updateOrder(id,order);
        return true;
    }
    @GetMapping("/{id}")
    @ResponseBody
    public Order getByID(@PathVariable Integer id)throws IdNotExistException{
        return orderService.findById(id);
    }
    @GetMapping("/passenger/{id}")
    @ResponseBody
    public List<Order> getByPassenger(@PathVariable Integer id)throws IdNotExistException{
        return orderService.findByPassenger(id);
    }
    @GetMapping("/driver/{id}")
    @ResponseBody
    public List<Order> getByDriver(@PathVariable Integer id)throws IdNotExistException{
        return orderService.findByDriver(id);
    }
    @GetMapping("/passenger/current/{id}")
    @ResponseBody
    public Order getCurrentByPassenger(@PathVariable Integer id){
        return passengerRepository.findById(id).getOrder();
    }
    @GetMapping("/driver/current/{id}")
    @ResponseBody
    public Order getCurrentByDriver(@PathVariable Integer id){
        return driverRepository.findById(id).getOrder();
    }
}
