package org.jlu.dede.dataaccess.Controller;

import org.jlu.dede.dataaccess.exceptions.IdNotExistException;
import org.jlu.dede.dataaccess.exceptions.NullBodyException;
import org.jlu.dede.dataaccess.model.BlackList;
import org.jlu.dede.dataaccess.service.BlackListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/blacklists")
public class BlackListController {
    @Autowired
    BlackListService blackListService;
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public void deleteBlackList(@PathVariable Integer id)throws IdNotExistException {
        blackListService.deleteById(id);
    }
    @RequestMapping(value = "/passenger/{passenger_id}/driver/{driver_id}",method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteBlackListByPerson(@PathVariable Integer passenger_id, @PathVariable Integer driver_id){
        blackListService.deleteByPerson(passenger_id,driver_id);
    }
    @GetMapping(value = "/passenger/{passenger_id}/driver/{driver_id}")
    @ResponseBody
    public BlackList findByPerson(@PathVariable Integer passenger_id, @PathVariable Integer driver_id){
        return blackListService.findByPerson(passenger_id,driver_id);
    }
    @PostMapping
    @ResponseBody
    public void addBlackList(@RequestBody BlackList blackList)throws NullBodyException{
        if(null == blackList){
            throw new NullBodyException();
        }
        blackListService.addBlackList(blackList);
    }
    @PostMapping("/{id}")
    @ResponseBody
    public void updateBlackList(@PathVariable Integer id,@RequestBody BlackList blackList)throws NullBodyException,IdNotExistException{
        if(null == blackList){
            throw new NullBodyException();
        }
        blackListService.updateBlackList(id,blackList);
    }
    @GetMapping("/{id}")
    @ResponseBody
    public BlackList getByID(@PathVariable Integer id)throws IdNotExistException{
        return blackListService.findById(id);
    }
    @GetMapping("/passengers/{passenger_id}")
    @ResponseBody
    public List<BlackList> getByPassenger(@PathVariable Integer passenger_id){
       return blackListService.passengerList(passenger_id);
    }
}
