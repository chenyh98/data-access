package org.jlu.dede.dataaccess.repository;

import org.jlu.dede.dataaccess.model.Driver;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DriverRepository extends JpaRepository<Driver,Long> {
    Driver findById(Integer id);
    void deleteById(Integer id);
    @Query("select driver from Driver driver where driver.account.id=:id")
    Driver findByAccount(@Param("id")Integer id);
    boolean existsById(Integer id);
    @Query("select driver from Driver driver where driver.order.id=:id")
    Driver findByOrder(@Param("id")Integer id);
}
