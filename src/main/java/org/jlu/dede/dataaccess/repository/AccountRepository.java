package org.jlu.dede.dataaccess.repository;
import org.jlu.dede.dataaccess.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account,Long> {
    Account findById(Integer id);
    void removeById(Integer id);
    Account findByEmail(String email);
    boolean existsById(Integer id);
}
