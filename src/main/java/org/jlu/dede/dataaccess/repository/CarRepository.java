package org.jlu.dede.dataaccess.repository;

import org.jlu.dede.dataaccess.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CarRepository extends JpaRepository<Car,Long> {
    Car findById(Integer id);
    void deleteById(Integer id);
    @Query("select car from Car car where car.driver.id = :id")
    List<Car> findByDriver(@Param("id") Integer id);
    boolean existsById(Integer id);
}
