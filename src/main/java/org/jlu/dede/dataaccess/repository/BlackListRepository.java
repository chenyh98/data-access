package org.jlu.dede.dataaccess.repository;

import org.jlu.dede.dataaccess.model.BlackList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BlackListRepository extends JpaRepository<BlackList,Long> {
    BlackList findById(Integer id);
    BlackList deleteById(Integer id);
    @Query("select blacklist from BlackList blacklist where blacklist.passenger.id = :id")
    List<BlackList> findByPassenger(@Param("id") Integer id);
    @Query("select blacklist from BlackList blacklist where blacklist.passenger.id = :pid and blacklist.driver.id=:did")
    BlackList findByPerson(@Param("pid") Integer passenger_id, @Param("did")Integer driver_id);
    @Modifying
    @Query("delete from BlackList blacklist where blacklist.passenger.id=:pid and blacklist.driver.id=:did")
    void deleteByPerson(@Param("pid")Integer passenger_id, @Param("did")Integer driver_id);
    boolean existsById(Integer id);
}
