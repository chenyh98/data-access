package org.jlu.dede.dataaccess.repository;

import org.jlu.dede.dataaccess.model.Record;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RecordRepository extends JpaRepository<Record,Long> {
    Record findById(Integer id);
    @Query("select record from Record record where record.payer.id=:id")
    List<Record> findByPayer(@Param("id") Integer id);
    //List<Record> findByPayer(Account account);
    boolean existsById(Integer id);
}
