package org.jlu.dede.dataaccess.repository;

import org.jlu.dede.dataaccess.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order,Long> {
    Order findById(Integer id);
    Order deleteById(Integer id);
    @Query("select order_list from Order order_list where order_list.driver.id = :id")
    List<Order> findByDriver(@Param("id") Integer id);
    @Query("select order_list from Order order_list where order_list.passenger.id = :id")
    List<Order> findByPassenger(@Param("id") Integer id);
    boolean existsById(Integer id);
}
