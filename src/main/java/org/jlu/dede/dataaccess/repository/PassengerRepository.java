package org.jlu.dede.dataaccess.repository;

import org.jlu.dede.dataaccess.model.Account;
import org.jlu.dede.dataaccess.model.Order;
import org.jlu.dede.dataaccess.model.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface PassengerRepository extends JpaRepository<Passenger,Long> {
    Passenger findById(Integer id);
    void deleteById(Integer id);
    @Query("select passenger from Passenger passenger where passenger.account.id=:id")
    Passenger findByAccount(@Param("id")Integer id);
    boolean existsById(Integer id);
    //Account findAccountById(Integer id);
}
