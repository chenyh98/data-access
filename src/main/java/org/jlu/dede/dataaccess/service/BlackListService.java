package org.jlu.dede.dataaccess.service;

import org.jlu.dede.dataaccess.exceptions.IdNotExistException;
import org.jlu.dede.dataaccess.model.BlackList;
import org.jlu.dede.dataaccess.repository.BlackListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class BlackListService {
    @Autowired
    BlackListRepository blackListRepository;
    public void deleteById(Integer id)throws IdNotExistException{
        if (!blackListRepository.existsById(id)){
            throw new IdNotExistException(id);
        }
        blackListRepository.deleteById(id);
    }
    public void addBlackList(BlackList blackList){
        blackListRepository.save(blackList);
    }
    public void updateBlackList(Integer id,BlackList blackList)throws IdNotExistException{
        if (!blackListRepository.existsById(id)){
            throw new IdNotExistException(id);
        }
        blackList.setId(id);
        blackListRepository.save(blackList);
    }
    public BlackList findById(Integer id)throws IdNotExistException{
        if (!blackListRepository.existsById(id)){
            throw new IdNotExistException(id);
        }
        return blackListRepository.findById(id);
    }
    public List<BlackList> passengerList(Integer id)throws IdNotExistException{
        return blackListRepository.findByPassenger(id);
    }
    public void deleteByPerson(Integer passenger_id, Integer driver_id){
        blackListRepository.deleteByPerson(passenger_id,driver_id);
    }
    public BlackList findByPerson(Integer pid,Integer did){
        return blackListRepository.findByPerson(pid,did);
    }
}
