package org.jlu.dede.dataaccess.service;


import org.jlu.dede.dataaccess.exceptions.IdNotExistException;
import org.jlu.dede.dataaccess.model.Car;
import org.jlu.dede.dataaccess.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class CarService {
    @Autowired
    CarRepository carRepository;
    public void deleteById(Integer id)throws IdNotExistException {
        if(!carRepository.existsById(id)){
            throw new IdNotExistException(id);
        }
        carRepository.deleteById(id);
    }
    public void addCar(Car car){
        carRepository.save(car);
    }
    public void updateCar(Integer id,Car car)throws IdNotExistException{
        if(!carRepository.existsById(id)){
            throw new IdNotExistException(id);
        }
        car.setId(id);
        carRepository.save(car);
    }
    public Car getById(Integer id)throws IdNotExistException{
        if(!carRepository.existsById(id)){
            throw new IdNotExistException(id);
        }
        return carRepository.findById(id);
    }
    public List<Car> findByDriver(Integer id){
        return carRepository.findByDriver(id);
    }
}
