package org.jlu.dede.dataaccess.service;

import com.codingapi.txlcn.tc.annotation.DTXPropagation;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import org.jlu.dede.dataaccess.daoutils.BeanUtilsExt;
import org.jlu.dede.dataaccess.exceptions.IdNotExistException;
import org.jlu.dede.dataaccess.model.Order;
import org.jlu.dede.dataaccess.repository.DriverRepository;
import org.jlu.dede.dataaccess.repository.OrderRepository;
import org.jlu.dede.dataaccess.repository.PassengerRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;

@Service
@Transactional
public class OrderService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    PassengerRepository passengerRepository;
    @Autowired
    DriverRepository driverRepository;
    @LcnTransaction(propagation = DTXPropagation.SUPPORTS)
    public Integer addOrder(Order order){
        orderRepository.save(order);
        return order.getId();
    }
    public void updateOrder(Integer id,Order order)throws IdNotExistException {
        if(!orderRepository.existsById(id)){
            throw new IdNotExistException(id);
        }
        Order old = orderRepository.findById(id);
        order.setId(id);
        BeanUtils.copyProperties(order,old, BeanUtilsExt.getNullPropertyNames(order));
        //old.setDriver(driverRepository.findByOrder(order.getId()));
        orderRepository.save(old);
    }
    public Order findById(Integer id)throws IdNotExistException{
        if(!orderRepository.existsById(id)){
            throw new IdNotExistException(id);
        }
        return orderRepository.findById(id);
    }
    public List<Order> findByPassenger(Integer id){
        return orderRepository.findByPassenger(id);
    }
    public List<Order> findByDriver(Integer id){
        return orderRepository.findByDriver(id);
    }
}
