package org.jlu.dede.dataaccess.service;

import com.codingapi.txlcn.tc.annotation.DTXPropagation;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import org.jlu.dede.dataaccess.exceptions.IdNotExistException;
import org.jlu.dede.dataaccess.model.Record;
import org.jlu.dede.dataaccess.repository.RecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class RecordService {
    @Autowired
    RecordRepository recordRepository;
    @LcnTransaction(propagation = DTXPropagation.SUPPORTS)
    public void addRecord(Record record){
        recordRepository.save(record);
    }
    public Record findById(Integer id)throws IdNotExistException {
        if(!recordRepository.existsById(id)){
            throw new IdNotExistException(id);
        }
        return recordRepository.findById(id);
    }
    public List<Record> findByPayer(Integer id){
        return recordRepository.findByPayer(id);
    }
}
