package org.jlu.dede.dataaccess.service;

import org.jlu.dede.dataaccess.daoutils.BeanUtilsExt;
import org.jlu.dede.dataaccess.exceptions.IdNotExistException;
import org.jlu.dede.dataaccess.model.Account;
import org.jlu.dede.dataaccess.model.Driver;
import org.jlu.dede.dataaccess.model.Order;
import org.jlu.dede.dataaccess.model.Passenger;
import org.jlu.dede.dataaccess.repository.AccountRepository;
import org.jlu.dede.dataaccess.repository.OrderRepository;
import org.jlu.dede.dataaccess.repository.PassengerRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class PassengerService {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    PassengerRepository passengerRepository;
    @Autowired
    OrderRepository orderRepository;
    public void addPassenger(Passenger passenger){
        passengerRepository.save(passenger);
    }
    public void updatePassenger(Integer id,Passenger passenger)throws IdNotExistException {
        if(!passengerRepository.existsById(id)){
            throw new IdNotExistException(id);
        }
        Passenger old = findById(id);
        passenger.setId(id);
        if(passenger.getAccount()!=null){
            Account newAccount = passenger.getAccount();
            Account oldAccount = accountRepository.findById(passengerRepository.findById(id).getAccount().getId());
            newAccount.setId(passengerRepository.findById(id).getAccount().getId());
            BeanUtils.copyProperties(newAccount,oldAccount, BeanUtilsExt.getNullPropertyNames(newAccount));
            accountRepository.save(oldAccount);
            passenger.setAccount(null);
        }
        BeanUtils.copyProperties(passenger,old, BeanUtilsExt.getNullPropertyNames(passenger));
        passengerRepository.save(old);
    }
    public Passenger findById(Integer id)throws IdNotExistException{
        if(!passengerRepository.existsById(id)){
            throw new IdNotExistException(id);
        }
        return passengerRepository.findById(id);
    }
    public Integer passengerOrder(Integer id, Order order)throws IdNotExistException{
        if(!passengerRepository.existsById(id)){
            throw new IdNotExistException(id);
        }
        Passenger passenger = passengerRepository.findById(id);
        if(order.getState() != -1) {
            passenger.setOrder(order);
            passengerRepository.save(passenger);
            Integer ooid = passenger.getOrder().getId();
            return ooid;
        }
        else {
            passenger.setOrder(null);
            passengerRepository.save(passenger);
            return 0;
        }

    }
    public Passenger findByAccount(Integer id){
        return passengerRepository.findByAccount(id);
    }
    public Passenger findByOrder(Integer id){
        return orderRepository.findById(id).getPassenger();
    }
}
