package org.jlu.dede.dataaccess.service;


import com.codingapi.txlcn.tc.annotation.DTXPropagation;
import com.codingapi.txlcn.tc.annotation.LcnTransaction;
import org.jlu.dede.dataaccess.daoutils.BeanUtilsExt;
import org.jlu.dede.dataaccess.exceptions.IdNotExistException;
import org.jlu.dede.dataaccess.model.Account;
import org.jlu.dede.dataaccess.repository.AccountRepository;
import org.jlu.dede.dataaccess.repository.DriverRepository;
import org.jlu.dede.dataaccess.repository.PassengerRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AccountService {
    @Autowired
    AccountRepository accountRepository;
    @Autowired
    PassengerRepository passengerRepository;
    @Autowired
    DriverRepository driverRepository;
    @LcnTransaction(propagation = DTXPropagation.SUPPORTS)
    public void save(Account account){
        accountRepository.save(account);
    }
    @LcnTransaction(propagation = DTXPropagation.SUPPORTS)
    public void updateAccount(Integer id,Account account)throws IdNotExistException{
        if(!accountRepository.existsById(id)){
            throw(new IdNotExistException(id));
        }
        Account old = accountRepository.findById(id);
        account.setId(id);
        BeanUtils.copyProperties(account,old, BeanUtilsExt.getNullPropertyNames(account));
        accountRepository.save(old);
    }
    public Account findById(Integer id)throws IdNotExistException{
        if(!accountRepository.existsById(id)){
            throw(new IdNotExistException(id));
        }
        return accountRepository.findById(id);
    }
    public Account findByEmail(String email){
        return accountRepository.findByEmail(email);
    }
    public Account findByPassenger(Integer id){
        return passengerRepository.findById(id).getAccount();
    }
    public Account findByDriver(Integer id){
        return driverRepository.findById(id).getAccount();
    }
}
