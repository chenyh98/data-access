package org.jlu.dede.dataaccess.service;

import org.jlu.dede.dataaccess.daoutils.BeanUtilsExt;
import org.jlu.dede.dataaccess.exceptions.IdNotExistException;
import org.jlu.dede.dataaccess.model.Account;
import org.jlu.dede.dataaccess.model.Car;
import org.jlu.dede.dataaccess.model.Driver;
import org.jlu.dede.dataaccess.model.Order;
import org.jlu.dede.dataaccess.repository.AccountRepository;
import org.jlu.dede.dataaccess.repository.CarRepository;
import org.jlu.dede.dataaccess.repository.DriverRepository;
import org.jlu.dede.dataaccess.repository.OrderRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class DriverService {
    @Autowired
    DriverRepository driverRepository;
    @Autowired
    CarRepository carRepository;
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    AccountRepository accountRepository;
    public void addDriver(Driver driver){
        driverRepository.save(driver);
    }
    public void updateDriver(Integer id,Driver driver)throws IdNotExistException {
        if(!driverRepository.existsById(id)){
            throw new IdNotExistException(id);
        }
        Driver old = findById(id);
        driver.setId(id);
        if(driver.getAccount()!=null){
            Account newAccount = driver.getAccount();
            Account oldAccount = accountRepository.findById(driverRepository.findById(id).getAccount().getId());
            newAccount.setId(driverRepository.findById(id).getAccount().getId());
            BeanUtils.copyProperties(newAccount,oldAccount, BeanUtilsExt.getNullPropertyNames(newAccount));
            accountRepository.save(oldAccount);
            driver.setAccount(null);
        }
        BeanUtils.copyProperties(driver,old, BeanUtilsExt.getNullPropertyNames(driver));
        driverRepository.save(old);
    }
    public Driver findById(Integer id)throws IdNotExistException {
        if(!driverRepository.existsById(id)){
            IdNotExistException ex =new IdNotExistException(id);
            throw ex;
        }
        return driverRepository.findById(id);
    }
    public void driverOrder(Integer id, Order order)throws IdNotExistException {
        if(!driverRepository.existsById(id)){
            throw new IdNotExistException(id);
        }
        //Order order = orderRepository.findById(oid);
        Driver driver = driverRepository.findById(id);
        if(order.getState() != -1&&order.getState()!=3)
            driver.setOrder(order);
        else
            driver.setOrder(null);
        driverRepository.save(driver);
    }
    public void chooseCar(Integer id, Car car){
        Driver driver = driverRepository.findById(id);
        Car car1 = carRepository.findById(car.getId());
        driver.setCurrentCar(car1);
        driverRepository.save(driver);
    }
    public void updateState(Integer id,String state)throws IdNotExistException {
        if(!driverRepository.existsById(id)){
            throw new IdNotExistException(id);
        }
        Driver driver = driverRepository.findById(id);
        driver.setState(state);
        driverRepository.save(driver);
    }
    public Driver findByAccount(Integer id){
        return driverRepository.findByAccount(id);
    }
    public Driver findByOrder(Integer id){
       return orderRepository.findById(id).getDriver();
    }
}
