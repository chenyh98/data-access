package org.jlu.dede.dataaccess;


//import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import com.codingapi.txlcn.tc.config.EnableDistributedTransaction;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "org.jlu.dede.dataaccess.repository")
//@EnableDistributedTransaction
public class DedeDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(DedeDataApplication.class, args);
    }

}
