package org.jlu.dede.dataaccess.exceptions;

import org.jlu.dede.publicUtlis.json.RestException;

public class NullBodyException extends RestException {
    public NullBodyException(){
        super(720,"RequestBody cannot be null");
        setErrorCode(Integer.valueOf(720));
    }
}
