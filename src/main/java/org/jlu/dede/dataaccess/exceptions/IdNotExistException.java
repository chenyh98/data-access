package org.jlu.dede.dataaccess.exceptions;

import org.jlu.dede.publicUtlis.json.RestException;

public class IdNotExistException extends RestException {
    private Integer id;
    public IdNotExistException(Integer id){
        super(Integer.valueOf(721),"No such object id="+String.valueOf(id));
        setErrorCode(Integer.valueOf(721));
    }
}
