package org.jlu.dede.dataaccess.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;

@JsonIgnoreProperties(value={"hibernateLazyInitializer", "handler"})
@Entity
@Data
@Table
@DynamicUpdate(true)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Passenger implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String x;
    String y;
    String certificate;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToOne(fetch= FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "account",referencedColumnName = "id",nullable = true)
    Account account;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToOne(fetch= FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.MERGE})
    @JoinColumn(name = "currentorder",referencedColumnName = "id",nullable = true)
    Order order;
    String homeAddress;
    String companyAddress;
    String commonAddressOne;
    String commonAddressTwo;
}

