package org.jlu.dede.dataaccess.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@JsonIgnoreProperties(value={"hibernateLazyInitializer", "handler"})
@Entity
@Data
@Table(name = "order_list")
@DynamicUpdate
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id",scope = Order.class)
public class Order implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    int type;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne
    @JoinColumn(insertable = true)
    Passenger passenger;//乘客外键
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne
    @JoinColumn(insertable = true)
    Driver driver;//司机外键
    String createTime;
    String acceptTime;
    String driverArriveTime;
    String departTime;
    String arriveDestinationTime;
    String payTime;
    double vetifyValue;
    BigDecimal finalMoney;
    String idealDepartSite;
    String idealDestinationSite;
    String actualDepartSite;
    String actualDestinationSite;
    int state;
    String startTime;
    String orderTime;
    double distance;
    @OneToOne
   // @PrimaryKeyJoinColumn(name="RECORD_ID")
            Record record;//交易记录外键
}