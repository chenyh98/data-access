package org.jlu.dede.dataaccess.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@JsonIgnoreProperties(value={"hibernateLazyInitializer", "handler"})
@Entity
@Data
@Table
@DynamicUpdate
public class Account implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String name;
    int age;
    String sex;
    String password;
    String phone;
    String email;
    String registerTime;
    double verifyValue;
    BigDecimal money;
    int type;
    int state;
}
