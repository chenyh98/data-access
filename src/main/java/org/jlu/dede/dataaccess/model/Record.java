package org.jlu.dede.dataaccess.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@JsonIgnoreProperties(value={"hibernateLazyInitializer", "handler"})
@Entity
@Data
@Table
@DynamicUpdate
public class Record implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToOne(fetch= FetchType.LAZY,cascade = CascadeType.MERGE)
    @JoinColumn(name = "payer",referencedColumnName = "id",nullable = true)
    Account payer;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @OneToOne(fetch= FetchType.LAZY,cascade = CascadeType.MERGE)
    @JoinColumn(name = "accepter",referencedColumnName = "id",nullable = true)
    Account accepter;
    BigDecimal money;
    String tradeTime;
    int type;
}
